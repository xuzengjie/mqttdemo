﻿using MQTTService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace 测试
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        MQTTServices service = new MQTTServices();

        [Obsolete]
        private void Form1_Load(object sender, EventArgs e)
        {
            service.ClientConnecteds += new MQTTServices.ClientConnected(ClientConnected);
            service.ClientDisconnecteds += new MQTTServices.ClientDisconnected(ClientDisconnected);
            service.MessageReceiveds += new MQTTServices.MessageReceived(MessageReceived);
            service.StartMQTT(9900,5000);
        }
        private void ClientConnected(string ClientId)
        {
            try
            {
                this.Invoke((EventHandler)delegate
                {
                    if (string.IsNullOrEmpty(textBox1.Text))
                    {
                        textBox1.Text += $@"客户端>{ClientId}";
                    }
                    else
                    {
                        textBox1.Text += $@"{Environment.NewLine}客户端>{ClientId}";
                    }
                });
            }
            catch (Exception ex)
            {
            }
            
       
        }
        private void ClientDisconnected(string ClientId)
        {

            try
            {
                this.Invoke((EventHandler)delegate
                {
                    if (string.IsNullOrEmpty(textBox1.Text))
                    {
                        textBox1.Text += $@"客户端>{ClientId}>>已断线";
                    }
                    else
                    {
                        textBox1.Text += $@"{Environment.NewLine}客户端>{ClientId}>>已断线";
                    }
                });
            }
            catch (Exception ex)
            {

            }
           
        }
        private void MessageReceived(string ClientId, string Topic, string Payload, string QualityOfServiceLevel, bool Retain)
        {
            try
            {
                this.Invoke((EventHandler)delegate
                {
                    if (string.IsNullOrEmpty(textBox1.Text))
                    {
                        textBox1.Text += $@"客户端>{ClientId}>>发送主题>{Environment.NewLine}{Topic}>>发送消息体>{Environment.NewLine}{Payload}>>QOS>{QualityOfServiceLevel}";
                    }
                    else
                    {
                        textBox1.Text += $@"{Environment.NewLine}客户端>{ClientId}>>发送主题>{Environment.NewLine}{Topic}>>发送消息体>{Environment.NewLine}{Payload}>>QOS>{QualityOfServiceLevel}";
                    }
                });
            }
            catch (Exception ex)
            {

            }
            
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            service.StopMQTT();
        }
    }
}
