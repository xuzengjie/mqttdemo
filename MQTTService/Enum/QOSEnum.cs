﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MQTTService
{
    public enum QOSEnum
    {
        /// <summary>
        /// 至多发送一次
        /// </summary>
        AtMostOnce = 0,
        /// <summary>
        /// 至少发送一次
        /// </summary>
        AtLeastOnce = 1,
        /// <summary>
        /// 整好发送一次
        /// </summary>
        ExactlyOnce = 2
    }
}
