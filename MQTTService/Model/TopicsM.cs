﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MQTTService
{
    public class TopicsM
    {
        /// <summary>
        /// 是否使用本主题
        /// </summary>
        public bool IsCheck { get; set; }
        /// <summary>
        /// 推送/订阅主题
        /// </summary>
        public string Topics { get; set; }
        /// <summary>
        /// 要推送的消息体
        /// </summary>
        public string Describe { get; set; }
        /// <summary>
        /// 推送次数
        /// </summary>
        public QOSEnum QOSEnum { get; set; }
        /// <summary>
        /// 保留 默认为否
        /// </summary>
        public bool Retain { get; set; } = false;
    }
}
