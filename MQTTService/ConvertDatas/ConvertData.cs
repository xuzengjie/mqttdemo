﻿using MQTTnet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MQTTService.ConvertDatas
{
    public class ConvertData
    {
        [Obsolete]
        public static List<TopicFilter> ConvertTopicFilter(List<TopicsM> list)
        {
            List<TopicFilter> topics = null;
            topics =new List<TopicFilter>();
            List<TopicsM> tops = list.Where(s => s.IsCheck = true).ToList();
            foreach (var item in tops)
            {
                topics.Add(new TopicFilter()
                {
                    Topic = item.Topics,
                    QualityOfServiceLevel = (MQTTnet.Protocol.MqttQualityOfServiceLevel)item.QOSEnum
                });
            }
            return topics;
        }
    }
}
