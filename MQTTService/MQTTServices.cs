﻿using MQTTnet;
using MQTTnet.Client.Receiving;
using MQTTnet.Protocol;
using MQTTnet.Server;
using System;
using System.Text;

namespace MQTTService
{
    public class MQTTServices
    {
        private IMqttServer server;
        /// <summary>
        /// 连接回调
        /// </summary>
        /// <param name="ClientId">客户端连接Id</param>
        /// <param name="Version">版本号</param>
        public delegate void ClientConnected(string ClientId);
        public ClientConnected ClientConnecteds;
        /// <summary>
        /// 断线回调
        /// </summary>
        /// <param name="ClientId">客户端连接Id</param>
        public delegate void ClientDisconnected(string ClientId);
        public ClientDisconnected ClientDisconnecteds;
        /// <summary>
        /// 消息负荷
        /// </summary>
        /// <param name="ClientId">客户端连接Id</param>
        /// <param name="Topic">消息主题</param>
        /// <param name="Payload">消息体</param>
        /// <param name="QualityOfServiceLevel">QOS</param>
        /// <param name="Retain">保留</param>
        public delegate void MessageReceived(string ClientId, string Topic, string Payload, string QualityOfServiceLevel, bool Retain);
        public MessageReceived MessageReceiveds;
        #region 启动按钮事件
        [Obsolete]
        public void StartMQTT(int Port,int OutTime)
        {
            //构建Server配置项
            var optionBuilder = new MqttServerOptionsBuilder().WithDefaultEndpointBoundIPAddress(System.Net.IPAddress.Any).WithDefaultEndpointPort(Port).WithDefaultCommunicationTimeout(TimeSpan.FromMilliseconds(OutTime)).WithConnectionValidator(t =>
            {
                if (t.ClientId == "drzk001")
                {
                    if (t.Username != "drzk001" || t.Password != "drzk001")
                    {
                        t.ReturnCode = MqttConnectReturnCode.ConnectionRefusedBadUsernameOrPassword;
                    }
                }
                t.ReturnCode = MqttConnectReturnCode.ConnectionAccepted;
            });
            var option = optionBuilder.Build();

            server = new MqttFactory().CreateMqttServer();
            server.ApplicationMessageReceivedHandler = new MqttApplicationMessageReceivedHandlerDelegate(MqttServer_ApplicationMessageReceived);//绑定消息接收事件
            server.ClientConnectedHandler = new MqttServerClientConnectedHandlerDelegate(MqttServer_ClientConnected);//绑定客户端连接事件
            server.ClientDisconnectedHandler = new MqttServerClientDisconnectedHandlerDelegate(MqttServer_ClientDisconnected);//绑定客户端断开事件
            server.ClientSubscribedTopicHandler = new MqttServerClientSubscribedHandlerDelegate(Server_ClientSubscribedTopic);//绑定客户端订阅主题事件
            server.ClientUnsubscribedTopicHandler = new MqttServerClientUnsubscribedTopicHandlerDelegate(Server_ClientUnsubscribedTopic);//绑定客户端退订主题事件
            
            //启动
            server.StartAsync(option);

        }
        #endregion

        #region 停止MQTT
        public void StopMQTT()
        {
            if (server != null)
            {
                server.StopAsync();
            }
        }
        #endregion

        private void MqttServer_ClientConnected(MqttServerClientConnectedEventArgs e)
        {

            ClientConnecteds(e.ClientId);
            Console.WriteLine($"客户端[{e.ClientId}]已连接");
        }

        private void MqttServer_ClientDisconnected(MqttServerClientDisconnectedEventArgs e)
        {
            ClientDisconnecteds(e.ClientId);
            Console.WriteLine($"客户端[{e.ClientId}]已断开连接！");
        }

        private void MqttServer_ApplicationMessageReceived(MqttApplicationMessageReceivedEventArgs e)
        {
            MessageReceiveds(e.ClientId, e.ApplicationMessage.Topic, Encoding.UTF8.GetString(e.ApplicationMessage.Payload), e.ApplicationMessage.QualityOfServiceLevel.ToString(), e.ApplicationMessage.Retain);
            Console.WriteLine($"客户端[{e.ClientId}]>> 主题：{e.ApplicationMessage.Topic} 负荷：{Encoding.UTF8.GetString(e.ApplicationMessage.Payload)} Qos：{e.ApplicationMessage.QualityOfServiceLevel} 保留：{e.ApplicationMessage.Retain}");
        }
        private void Server_ClientSubscribedTopic(MqttServerClientSubscribedTopicEventArgs e)
        {

        }
        private void Server_ClientUnsubscribedTopic(MqttServerClientUnsubscribedTopicEventArgs e)
        {

        }
    }
}
